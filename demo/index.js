import angular from 'angular';
import planningModule from '../src/index';
import planningViewerModule from './planningViewer/planningViewer.module';
import appointmentModule from './appointment/appointment.module';
import transportModule from './transport/transport.module';
import floorplanModule from './floorplan/floorplan.module';

const myController = function () {};

angular.module('my-demo', [planningModule, planningViewerModule, appointmentModule, transportModule, floorplanModule])
    .controller('myCtrl', myController);
