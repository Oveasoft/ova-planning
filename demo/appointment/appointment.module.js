import angular from 'angular';

// Components
import AppointmentComponent from './appointment.component';

export default angular.module('appointment', [])
    .component('myAppointment', AppointmentComponent)
    .name;
