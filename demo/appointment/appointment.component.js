import './appointment.scss';
import template from './appointment.template.html';

class AppointmentComponent {
    constructor ($mdDialog) {
        this.$mdDialog = $mdDialog;
    }

    $onInit () {
        this.event = this.appointment;
    }

    /**
     * Triggered whenever you click on an appointment.
     */
    onClick () {
        this.appointment.date.add(10, 'minutes');
    }
}

export default {
    controller: AppointmentComponent,
    template,
    bindings: {
        appointment: '<'
    }
}
