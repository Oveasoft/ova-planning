import './floorplan.scss';
import template from './floorplan.template.html';

/**
 * @class FloorplanComponent
 * @property {Object} $element
 * @property {Object} data
 */
class FloorplanComponent {
    constructor ($element) {
        this.$element = $element;
    }

    $onInit () {
        if (this.data.valid) {
            this.$element.addClass('valid');
        }
    }

    onClick () {
        console.log(this.data);
    }
}

/**
 * @directive Floorplan
 * @name Floorplan
 * @param {Object} data
 */
export default {
    controller: FloorplanComponent,
    template,
    bindings: { data: '<' }
};
