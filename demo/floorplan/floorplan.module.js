import angular from 'angular';

// Components
import FloorplanComponent from './floorplan.component';

export default angular.module('floorplan', [])
    .component('floorplan', FloorplanComponent)
    .name;
