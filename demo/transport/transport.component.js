import './transport.scss';
import template from './transport.template.html';

/**
 * @
 * @name TransportComponent
 * @class TransportComponent
 * @param {Object} transport
 */
class TransportComponent {
    constructor ($element) {
        this.$element = $element;
    }

    $onInit () {
        this.event = this.transport;

        if (this.event.id % 5) {
            this.$element.addClass('red');
        } else if (this.event.id % 2) {
            this.$element.addClass('green');
        }
    }

    /**
     * Triggered whenever you click on a transport.
     */
    onClick () {
        console.log(this.event);
    }
}

export default {
    controller: TransportComponent,
    template,
    bindings: {
        transport: '<'
    }
};
