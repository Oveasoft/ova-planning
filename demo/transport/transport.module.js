import angular from 'angular';

// Components
import TransportComponent from './transport.component';

export default angular.module('transport', [])
    .component('transport', TransportComponent)
    .name;
