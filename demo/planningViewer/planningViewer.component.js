import './planningViewer.scss';
import template from './planningViewer.template.html';
import moment from 'moment';
import _ from 'lodash';

class PlanningViewerComponent {
    constructor ($timeout) {
        this.$timeout = $timeout;
        this.loading = false;
        this._buildRandomEvent = (week, configuration) => {
            return {
                id: _.uniqueId(),
                date: moment().week(_.random(week.get('week') - 2, week.get('week') + 2)).day(_.random(1, 6)).hour(_.random(1, 23)).minute(_.random(0, 59)).second(_.random(0, 59)),
                label: `Peugeot ${_.random(300, 499)}`,
                preparationStates: {
                    a: _.random(0, 1),
                    b: _.random(0, 1),
                    c: _.random(0, 1),
                    d: _.random(0, 1),
                    e: _.random(0, 1),
                    f: _.random(0, 1)
                }
            };
        };

        this._floorplanBuilder = date => {
            const customers = ['PSA', 'Renault-Nissan', 'FCA', 'Ford', 'Volkswagen AG'];
            return {
                date,
                customer: customers[_.random(0, customers.length - 1)],
                label: `D${date.day()}W${date.week()}`,
                valid: _.random(),
                amount: _.random(-100000, 100000)
            };
        };

        this._getEvents = (quantity, week, configuration) => {
            let events = [];

            if (this.configuration.viewer === 'trimester') {
                const firstCol = moment(configuration.currentWeek).subtract(configuration.views.trimester.range.past, configuration.views.trimester.columnType);
                const lastCol = moment(firstCol).add(configuration.views.trimester.range.future + configuration.views.trimester.range.past, configuration.views.trimester.columnType);
                const cursor = moment(firstCol);
                while (cursor.isSameOrBefore(lastCol)) {
                    events.push(this._floorplanBuilder(moment(cursor)));
                    cursor.add(1, 'day');
                }
            } else {
                for (let i = 0; i < quantity; i++) {
                    events.push(this._buildRandomEvent(week, configuration));
                }

                // Lundi 14h
                events.push({
                    id: _.uniqueId(),
                    date: moment(week).day(1).hour(14).minute(0).second(0),
                    label: `RDV de 14h`,
                    preparationStates: {
                        a: _.random(0, 1),
                        b: _.random(0, 1),
                        c: _.random(0, 1),
                        d: _.random(0, 1),
                        e: _.random(0, 1),
                        f: _.random(0, 1)
                    }
                });
            }

            return events;
        };

        this.dataProviders = {
            api: {},
            random: (configuration) => this.fetchData(configuration)
        };
    }

    $onInit () {
        this.selected = null;
        this.rangeNumber = 1;
        this.rangeType = 'week';
        this.fetchNewData = true;
        this.previousWeek = null;

        this.configuration = {
            momentLocale: 'fr',
            datePath: 'date',
            debug: true,
            currentWeek: moment(),
            showControls: true,
            showLabel: true,
            showViewSwapper: true,
            excludedDays: [],
            viewer: 'trimester',
            onLoad: data => {
                this.informations = data;
            },
            informations: this.informations,
            onChangeWeek: week => {
                this.configuration.currentWeek.week(week.week()).year(week.year());
                this.fetchData(this.configuration);
            },
            onClick: appointment => {
                this.selected = appointment;
                appointment.date.add(10, 'minute');
            },
            selectSlot: slot => {
                console.log('slot', slot);
                this.slot = moment(slot.date);
            },
            views: {
                week: {
                    template: () => `<transport transport="$appointment"></transport>`
                },
                month: {
                    weeksPerView: {
                        default: 4,
                        current: 4
                    },
                    template: () => `<transport transport="$appointment"></transport>`
                },
                trimester: {
                    columnType: 'month',
                    range: {
                        past: 2,
                        future: 2
                    },
                    labels: date => date.format('MMMM YYYY'),
                    template: () => `<floorplan data="$appointment"></floorplan>`
                }

            }
        };

        this.dataProviders.random(this.configuration);
    }

    fetchData (configuration) {
        this.loading = true;
        let promise = new Promise((resolve) => {
            resolve(this._getEvents(500, moment(configuration.currentWeek), configuration));
        });

        promise.then((value) => {
            this.$timeout(() => {
                this.events = value;
                this.loading = false;
            }, 400);
        });
    }

    previous () {
        this.configuration.currentWeek.subtract(this.rangeNumber, this.rangeType);
        if (this.fetchNewData) {
            this.fetchData(this.configuration);
        }
    }

    next () {
        this.configuration.currentWeek.add(this.rangeNumber, this.rangeType);
        if (this.fetchNewData) {
            this.fetchData(this.configuration);
        }
    }
}

export default {
    controller: PlanningViewerComponent,
    template
};
