import angular from 'angular';

// Components
import PlanningViewerComponent from './planningViewer.component';

export default angular.module('planning-viewer', [])
    .component('planningViewer', PlanningViewerComponent)
    .name;
