import './ovaPlanningSlot.scss';
import _ from 'lodash';

/**
 *
 * @class OvaPlanningSlotComponent
 */
class OvaPlanningSlotComponent {
    constructor ($compile, $element, $scope, $timeout) {
        this.$scope = $scope;
        this.$element = $element;
        this.$compile = $compile;
        this.$timeout = $timeout;
    }

    $onInit () {
        /**
         * All keys must be equal to true to pass validation.
         * @type {{isBefore: boolean, isSameDay: boolean}}
         * @private
         */
        this._validationRules = {
            isBefore: this.slot.start.isBefore(this.slot.end),
            isSameDay: this.slot.start.isSame(this.slot.end, 'day')
        };

        /**
         * Check validation rules.
         */
        _.each(this._validationRules, (rule, key) => {
            if (!rule) {
                throw new Error(`Failed to pass validation rule '${key}'`);
            }
        });

        this.slot.data.forEach(appointment => this.compileTemplate(appointment));
    }

    compileTemplate (appointment) {
        this.$scope.$appointment = appointment;
        const elt = this.ovaPlanning.config.views.week.template(appointment);

        const htmlAppointment = this.$compile(elt)(this.$scope);

        if (this.ovaPlanning.config.onClick) {
            htmlAppointment.bind('click', () => {
                this.$timeout(() => {
                    this.ovaPlanning.config.onClick(appointment);
                });
            });
        }

        this.$element.append(htmlAppointment);
    }
}

export default {
    controller: OvaPlanningSlotComponent,
    require: {
        ovaPlanning: '^ovaPlanning'
    },
    bindings: {
        slot: '='
    }
};
