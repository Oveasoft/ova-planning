import _ from 'lodash';
import angular from 'angular';
import moment from 'moment';
import { Debugger } from './debugger.class';

const ERROR_NAMESPACE = 'Configuration';

/**
 * Configuration class
 *
 * @export
 * @class Configuration
 * @description The configuration used by the planning, each values can be override.
 */
export class Configuration {
    /**
     * Creates an instance of Configuration.
     *
     * @param {*} override
     * @memberof Configuration
     * @param {string} _momentLocale Define the localization of the planning.
     * @param {boolean|null} [_showPause] Show the pauses.
     * @param {boolean|null} [_showControls] Show controls.
     * @param {boolean|null} [_showLabel] Show label.
     * @param {Array<number|null>} [_excludedDays] Exclude given days index from the planning. (ex: [0, 4] exclude Sunday and Thursday)
     * @param {Array<Array<number>|null>} [_slot] Define the range of the slot (format H24)
     */
    constructor (override) {
        this.Debugger = new Debugger(true);

        this._momentLocale = 'fr';
        this._datePath = 'date';
        this._showPause = null;
        this._showControls = null;
        this._showLabel = null;
        this._showViewSwapper = null;
        this._excludedDays = [];
        this._slot = [[8, 12], [14, 18]];
        this._slotInterval = 45;
        this._editOnClick = null;
        this._onChangeWeek = () => {};
        this._dayLabelFormat = 'ddd DD';
        this._theme = 'blue';
        this._viewer = 'week';
        this._debug = null;
        this._currentWeek = moment();
        this._template = () => {};
        this._onClick = () => {};
        this._onLoad = {};
        this._selectSlot = () => {};
        this.views = {
            week: {
                weeksPerView: {
                    default: 4,
                    current: 4
                },
                template: () => {}
            },
            month: {
                template: () => {}
            }
        };

        this._override = (override) => {
            this.momentLocale = override.momentLocale || this._momentLocale;
            this.datePath = override.datePath || this._datePath;
            this.showPause = override.showPause || false;
            this.showControls = override.showControls || false;
            this.showLabel = override.showLabel || false;
            this.showViewSwapper = override.showViewSwapper || false;
            this.excludedDays = override.excludedDays || this._excludedDays;
            this.slot = override.slot || this._slot;
            this.slotInterval = override.slotInterval || this._slotInterval;
            this.editOnClick = override.editOnClick || false;
            this.onChangeWeek = override.onChangeWeek || this._onChangeWeek;
            this.dayLabelFormat = override.dayLabelFormat || this._dayLabelFormat;
            this.theme = override.theme || this._theme;
            this.viewer = override.viewer || this._viewer;
            this.debug = override.debug || false;
            this.currentWeek = override.currentWeek || this._currentWeek;
            this.template = override.template || this._template;
            this.onClick = override.onClick || this._onClick;
            this.onLoad = override.onLoad || this._onLoad;
            this.selectSlot = override.selectSlot || this._selectSlot;
        };

        this._typeError = (field, type, given) => {
            this.Debugger.error(`${ERROR_NAMESPACE} : ${field} must be a ${type}. (${typeof given} given)`);
        };

        if (override) {
            this._override(override);
            _.merge(this.views, override.views);
        }
    }

    /**
     * Return the momentLocale of the planning.
     *
     * @memberof Configuration
     */
    get momentLocale () {
        return this._momentLocale;
    }

    /**
     * Set the momentLocale of the planning.
     *
     * @memberof Configuration
     */
    set momentLocale (value) {
        if (angular.isString(value)) {
            this._momentLocale = value;
        } else {
            this._typeError('momentLocale', 'string', value);
        }
    }

    /**
     * Set the date path.
     *
     * @returns {string}
     */
    get datePath () {
        return this._datePath;
    }

    /**
     * Get the date path.
     *
     * @param value
     */
    set datePath (value) {
        if (angular.isString(value)) {
            this._datePath = value;
        } else {
            this._typeError('datePath', 'string', value);
        }
    }

    /**
     * Return if breaks line are shown on the planning.
     *
     * @memberof Configuration
     */
    get showPause () {
        return this._showPause;
    }

    /**
     * Set the visibility of the breaks line.
     *
     * @memberof Configuration
     */
    set showPause (value) {
        if (typeof value === 'boolean') {
            this._showPause = value;
        } else {
            this._typeError('showPause', 'boolean', value);
        }
    }

    /**
     * Return whether labels are shown or not.
     *
     * @memberof Configuration
     */
    get showLabel () {
        return this._showLabel;
    }

    /**
     * Set whether labels are shown or not.
     *
     * @memberof Configuration
     */
    set showLabel (value) {
        if (typeof value === 'boolean') {
            this._showLabel = value;
        } else {
            this._typeError('showLabel', 'boolean', value);
        }
    }

    /**
     * Return whether the button for swapping view is shown or not.
     *
     * @returns {null}
     */
    get showViewSwapper () {
        return this._showViewSwapper;
    }

    /**
     * Set whether the button for swapping view is shown or not.
     *
     * @param value
     */
    set showViewSwapper (value) {
        if (typeof value === 'boolean') {
            this._showViewSwapper = value;
        } else {
            this._typeError('showViewSwap', 'boolean', value);
        }
    }

    /**
     * Get excluded days.
     *
     * @memberof Configuration
     */
    get excludedDays () {
        return this._excludedDays;
    }

    /**
     * Set excluded days.
     *
     * @memberof Configuration
     */
    set excludedDays (value) {
        let isOk = true;
        // Check that excludedDays is an object.
        if (!(typeof value === 'object')) {
            return this._typeError('excludedDays', 'object', value);
        }

        _.each(value, item => {
            // Check that item in excludedDays is an number.
            if (isNaN(item)) {
                isOk = false;
                return this._typeError('excludedDays item', 'number', item);
            }

            // Check that item in excludedDays is between 0 and 6.
            if (item < 0 || item > 6) {
                isOk = false;
                return this.Debugger.error(`${ERROR_NAMESPACE} : values in excludedDays must be between 0 and 6. (${item} given)`);
            }
        });

        if (isOk) {
            this._excludedDays = value;
        }
    }

    /**
     * Get slots.
     *
     * @memberof Configuration
     */
    get slot () {
        return this._slot;
    }

    /**
     * Set the slots.
     *
     * @memberof Configuration
     */
    set slot (value) {
        let isOk = true;
        // Check that slots is an object.
        if (!(typeof value === 'object')) {
            this._typeError('slot', 'object', value);
        }

        _.each(value, (slots, slotsIndex) => {
            // Check that slot have strictly two values.
            let valuesBySlot = 2;
            if (slots.length !== valuesBySlot) {
                isOk = false;
                this.Debugger.error(`${ERROR_NAMESPACE} : the slot ${slotsIndex + 1} must contain ${valuesBySlot} values. (${slots.length} given)`);
            }

            _.each(slots, (hour, index) => {
                // Check that hours in slot are well ordered.
                if (hour < slots[index - 1]) {
                    isOk = false;
                    this.Debugger.error(`${ERROR_NAMESPACE} : the slot ${slotsIndex + 1} is badly ordered. (${slots[index - 1]} must be after ${slots[index]})`);
                }

                // Check that hours are between 0 and 23
                if (hour < 0 || hour > 23) {
                    isOk = false;
                    this.Debugger.error(`${ERROR_NAMESPACE} : the slot ${slotsIndex + 1} contain an invalid hour. (${hour} given)`);
                }
            });
        });

        if (isOk) {
            this._slot = value;
        }
    }

    /**
     * Get the slots interval.
     *
     * @readonly
     * @memberof Configuration
     */
    get slotInterval () {
        return this._slotInterval;
    }

    /**
     * Set the slots interval.
     *
     * @memberof Configuration
     * @param {number} value The interval size in minutes.
     */
    set slotInterval (value) {
        // Slot interval must be a number.
        if (typeof value !== 'number') {
            return this._typeError('slotInterval', 'number', value);
        }

        // Slot interval must be greater than 0.
        if (value <= 0) {
            return this.Debugger.error(`${ERROR_NAMESPACE} : slotInterval must be greater than 0. (${value} given)`);
        }

        // Slot interval must be lesser than 1440.
        if (value >= 1440) {
            return this.Debugger.error(`${ERROR_NAMESPACE} : slotInterval must be lesser than 1440. (${value} given)`);
        }

        this._slotInterval = value;
    }

    /**
     * Return the edition through click status.
     *
     * @memberof Configuration
     */
    get editOnClick () {
        return this._editOnClick;
    }

    /**
     * Enable/disable the edition through click.
     *
     * @memberof Configuration
     */
    set editOnClick (value) {
        if (typeof value === 'boolean') {
            this._editOnClick = value;
        } else {
            this._typeError('editOnClick', 'boolean', value);
        }
    }

    /**
     * Get the week change callback.
     *
     * @memberof Configuration
     */
    get onChangeWeek () {
        return this._onChangeWeek;
    }

    /**
     * Set the week change callback.
     *
     * @memberof Configuration
     */
    set onChangeWeek (value) {
        if (typeof value === 'function') {
            this._onChangeWeek = value;
        } else {
            this._typeError('onChangeWeek', 'function', value);
        }
    }

    /**
     * Get the format of days label.
     */
    get dayLabelFormat () {
        return this._dayLabelFormat;
    }

    /**
     * Set the format of days label.
     *
     * @memberof Configuration
     * @description Set the date format of the header of each day.
     */
    set dayLabelFormat (value) {
        if (typeof value === 'string') {
            this._dayLabelFormat = value;
        } else {
            this._typeError('dayLabelFormat', 'string', value);
        }
    }

    /**
     * Get the theme.
     *
     * @memberof Configuration
     */
    get theme () {
        return this._theme;
    }

    /**
     * Set the theme.
     *
     * @memberof Configuration
     * @description The theme will change the base color of the planning's template.
     */
    set theme (value) {
        if (typeof value === 'string') {
            this._theme = value;
        } else {
            this._typeError('theme', 'string', value);
        }
    }

    /**
     * Get the viewer.
     *
     * @returns {string}
     */
    get viewer () {
        return this._viewer;
    }

    /**
     * Set the viewer.
     *
     * @param value
     */
    set viewer (value) {
        if (typeof value === 'string') {
            this._viewer = value;
        } else {
            this._typeError('viewer', 'string', value);
        }
    }

    /**
     * Get the debug mode.
     *
     * @memberof Configuration
     */
    get debug () {
        return this._debug;
    }

    /**
     * Set the debug mode.
     *
     * @memberof Configuration
     * @description If set to true, the debug mode will show additionnal debug content on the user interface and the console.
     */
    set debug (value) {
        if (typeof value === 'boolean') {
            this._debug = value;
        } else {
            this._typeError('debug', 'boolean', value);
        }
    }

    /**
     * Get the current week.
     *
     * @memberof Configuration
     */
    get currentWeek () {
        return this._currentWeek;
    }

    /**
     * Set the current week.
     *
     * @memberof Configuration
     * @description Set the current viewed week on the planning.
     */
    set currentWeek (value) {
        if (moment(value).isValid()) {
            value = moment(value);
            value.locale(this.momentLocale);
            this._currentWeek = value;
        } else {
            this._typeError('currentWeek', 'moment friendly value', value);
        }
    }

    /**
     * Get the template
     *
     * @memberof Configuration
     */
    get template () {
        return this._template;
    }

    /**
     * Set the template
     *
     * @memberof Configuration
     */
    set template (value) {
        this._template = value;
    }

    get onClick () {
        return this._onClick;
    }

    set onClick (value) {
        this._onClick = value;
    }

    get onLoad () {
        return this._onLoad;
    }

    set onLoad (value) {
        this._onLoad = value;
    }

    /**
     * Get the selectedSlot function.
     *
     * @memberof Configuration
     */
    get selectSlot () {
        return this._selectSlot;
    }

    /**
     * Set the selectedSlot function.
     *
     * @memberof Configuration
     */
    set selectSlot (value) {
        this._selectSlot = value;
    }

    backToPRevious () {

    }
}
