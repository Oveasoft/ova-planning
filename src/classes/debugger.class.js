/**
 * Class show some debug messages.
 *
 * @export
 * @class Debugger
 */
export class Debugger {
    constructor (active) {
        this.active = active;
    }

    output (msg, type = 'log', color = '#8888DD') {
        if (this.active) {
            if (typeof console[type] === 'function') {
                console[type](`%c[ova-planning] ${msg}`, `background: #222; color: ${color}; font-size: 13px; padding: 3px`);
            } else {
                console.error(`$log does not have ${type} method`);
            }
        }
    }

    error (msg) {
        const style = '#DD8888';
        this.output(msg, 'error', style);
    }
}
