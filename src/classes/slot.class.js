import moment from 'moment';

/**
 * @class Slot
 * @description A slot is an object that represents a moment of the day between two time slots.
 */
export class Slot {
    /**
     * @constructor
     * @param {moment.Moment} start
     * @param {moment.Moment} end
     * @param {Array} data
     * @param {String} state
     */
    constructor(start, end, data, state) {
        /**
         * The start time of the slot.
         * @type {moment.Moment}
         * @private
         */
        this._start = start;

        /**
         * The end time of the slot.
         * @type {moment.Moment}
         * @private
         */
        this._end = end;

        /**
         * Data to be contained in the slot.
         * @type {Array}
         * @private
         */
        this._data = data;

        /**
         * The state of the slot.
         * @type {String}
         * @private
         */
        this._state = state;

        /**
         * Check if the value is a moment object or can be a moment object.
         * @param {moment.Moment|String} value
         * @returns {boolean}
         * @private
         */
        this._isMoment = (value) => {
            return (value instanceof moment || moment(value).isValid());
        };

        /**
         * All keys must be equal to true to pass validation.
         * @type {{isBefore: boolean, isSameDay: boolean}}
         * @private
         */
        this._validationRules = {
            isBefore: this.start.isBefore(this.end),
            isSameDay: this.start.isSame(this.end, 'day')
        };

        /**
         * Check validation rules.
         */
        _.each(this._validationRules, (rule, key) => {
            if (!rule) {
                throw new Error(`Failed to pass validation rule '${key}'`);
            }
        });
    }

    /**
     * Get the starting date of the slot.
     * @returns {moment.Moment}
     */
    get start() {
        return this._start;
    }

    /**
     * Set the starting date of the slot.
     * @param {moment.Moment|String} value
     */
    set start(value) {
        if (this._isMoment(value)) {
            this._start = value;
        } else {
            throw new Error(`Value is not a moment object, ${typeof value} given.`);
        }
    }

    /**
     * Get the ending date of the slot.
     * @returns {moment.Moment}
     */
    get end() {
        return this._end;
    }

    /**
     * Set the ending date of the slot.
     * @param {moment.Moment|String} value
     */
    set end(value) {
        if (this._isMoment(value)) {
            this._end = value;
        } else {
            throw new Error(`Value is not a moment object, ${typeof value} given.`);
        }
    }

    /**
     * Get the data of the slot.
     * @returns {Array}
     */
    get data() {
        return this._data;
    }

    /**
     * Set the data of the slot.
     * @param {Array} value
     */
    set data(value) {
        this._data = value;
    }

    /**
     * Get the state of the slot.
     * @returns {String}
     */
    get state() {
        return this._state;
    }

    /**
     * Set the state of the slot.
     * @param value
     */
    set state(value) {
        this._state = value;
    }
}
