import moment from 'moment';

/**
 * Helper to build recurrent moment object.
 *
 * @export
 * @class MomentHelper
 */
export class MomentHelper {
    /**
     * Extract the decimal from a number.
     * @param {Number} number
     */
    static extractDecimal (number) {
        return (number % 1).toFixed(2);
    }

    /**
     * Build a slot.
     *
     * @static
     * @param {moment.Moment} date
     * @param {Number} boundary
     * @returns {moment.Moment}
     * @memberof MomentHelper
     */
    static buildSlot (date, boundary) {
        return moment(date).startOf('day').hour(boundary).minute(this.extractDecimal(boundary) * 100);
    }
}
