import angular from 'angular';
import ngSanitize from 'angular-sanitize';
import angularMaterial from 'angular-material';
import angularAnimate from 'angular-animate';
import angularAria from 'angular-aria';

import ovaPlanningComponent from './ova-planning/ovaPlanning.component';
import ovaPlanningDayComponent from './ova-planning-day/ovaPlanningDay.component';
import ovaPlanningSlotComponent from './ova-planning-slot/ovaPlanningSlot.component';
import ovaPlanningViewMonthComponent from './ova-planning-views/month/ovaPlanningViewMonth.component';
import ovaPlanningViewTrimesterComponent from './ova-planning-views/trimester/ovaPlanningViewTrimester.component';

import 'angular-material/angular-material.min.css';

const moduleName = 'ovaPlanning';

angular.module(moduleName, ['ngMaterial', ngSanitize, angularMaterial, angularAnimate, angularAria])
    .component('ovaPlanning', ovaPlanningComponent)
    .component('ovaPlanningDay', ovaPlanningDayComponent)
    .component('ovaPlanningSlot', ovaPlanningSlotComponent)
    .component('ovaPlanningViewMonth', ovaPlanningViewMonthComponent)
    .component('ovaPlanningViewTrimester', ovaPlanningViewTrimesterComponent);

export default moduleName;
