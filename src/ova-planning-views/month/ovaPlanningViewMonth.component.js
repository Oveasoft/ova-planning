/**
 * @module ovaPlanning
 * @class OvaPlanningViewMonthComponent
 * @param {[Object]} data
 */
class OvaPlanningViewMonthComponent {
    constructor ($compile, $element, $scope) {
        'ngInject';
        this.$compile = $compile;
        this.$element = $element;
        this.$scope = $scope;
    }

    $onInit () {
        this.$scope.$appointment = this.data;
        let template = this.ovaPlanning.config.views.month.template();
        this.$element.append(this.$compile(template)(this.$scope));
    }
}

export default {
    controller: OvaPlanningViewMonthComponent,
    require: {
        ovaPlanning: '^ovaPlanning'
    },
    bindings: {
        data: '<'
    }
};
