/**
 * @module ovaPlanning
 * @class OvaPlanningViewTrimesterComponent
 * @property {Object[]} data
 * @property {Object} ovaPlanning
 */
class OvaPlanningViewTrimesterComponent {
    constructor ($compile, $element, $scope, $timeout) {
        'ngInject';
        this.$compile = $compile;
        this.$element = $element;
        this.$scope = $scope;
        this.$timeout = $timeout;
    }

    $onInit () {
        this.$scope.$appointment = this.data;
        let template = this.ovaPlanning.config.views.trimester.template();
        const compiledHtml = this.$compile(template)(this.$scope);

        if (this.ovaPlanning.config.onClick) {
            compiledHtml.bind('click', () => {
                this.$timeout(() => {
                    this.ovaPlanning.config.onClick(this.data);
                });
            });
        }

        this.$element.append(compiledHtml);
    }
}

export default {
    controller: OvaPlanningViewTrimesterComponent,
    require: {
        ovaPlanning: '^ovaPlanning'
    },
    bindings: {
        data: '<'
    }
};
